variable "cluster_name" {
  description = "Name of the K8s cluster"
}

variable "gcp_zone" {
  description = "GCP zone, e.g. europe-west1-b (which must be in gcp_region)"
}

variable "initial_node_count" {
  description = "Number of worker VMs to initially create"
  default = 2
}