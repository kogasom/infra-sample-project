variable "name" {}

resource "google_container_cluster" "primary" {
  name = "${var.cluster_name}"
  zone = "${var.gcp_zone}"
  initial_node_count = "${var.initial_node_count}"
  /* legacy abac should be enabled to use helm provider, because
  ClusterRoleBinding resource isn't supported currently by terraform
  see: https://github.com/terraform-providers/terraform-provider-kubernetes/pull/73
  */
  enable_legacy_abac = true

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

