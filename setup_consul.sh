#!/bin/bash

CLUSTER1_CONTEXT=cluster1
CLUSTER2_CONTEXT=cluster2

# gather data
CLUSTER2_NODE_IP=`kubectl --context=$CLUSTER2_CONTEXT -n default describe node | grep InternalIP | head -n 1 | awk '{print $2}'`
CLUSTER2_NODE_PORT=`kubectl --context=$CLUSTER2_CONTEXT -n default describe svc consul-serfwan-tcp|grep NodePort | grep serfwan-tcp | grep -oP "[0-9]{5}"`
CLUSTER1_GOSSIP_KEY=`kubectl --context=$CLUSTER1_CONTEXT -n default get secret consul-gossip-key -o jsonpath={.data.gossip-key}`
CLUSTER2_GOSSIP_KEY=`kubectl --context=$CLUSTER2_CONTEXT -n default get secret consul-gossip-key -o jsonpath={.data.gossip-key}`

# install gossip keys
kubectl --context=$CLUSTER1_CONTEXT exec consul-0 -- consul keyring -install=$CLUSTER2_GOSSIP_KEY
kubectl --context=$CLUSTER2_CONTEXT exec consul-0 -- consul keyring -install=$CLUSTER1_GOSSIP_KEY

# join
kubectl --context=$CLUSTER1_CONTEXT exec consul-0 -- consul join -wan $CLUSTER2_NODE_IP:$CLUSTER2_NODE_PORT

# check
kubectl --context=$CLUSTER1_CONTEXT exec consul-0 -- curl -s http://localhost:8500/v1/catalog/datacenters
kubectl --context=$CLUSTER1_CONTEXT exec consul-0 -- curl -s http://localhost:8500/v1/catalog/nodes?dc=dc1
kubectl --context=$CLUSTER1_CONTEXT exec consul-0 -- curl -s http://localhost:8500/v1/catalog/nodes?dc=dc2