# Infra sample project

## Prerequisites

* GCloud account
* gcloud and kubectl
* terraform
* helm

or you can use the included Vagrant setup

## Setup

* Set your gcloud project ID in ```terraform.tfvars```
* Set the service account: ```account.json```

```
terraform init
terraform plan
terraform apply
```

Wait for the clusters to be ready.

### Get cluster credentials
```
gcloud container clusters get-credentials cluster1 --zone europe-west1-b --project infra-sample-project

gcloud container clusters get-credentials cluster2 --zone us-west1-a --project infra-sample-project
```

### Rename contexts for convenience
```
kubectl config rename-context gke_infra-sample-project_europe-west1-b_cluster1 cluster1

kubectl config rename-context gke_infra-sample-project_us-west1-a_cluster2 cluster2
```

### Install helm/tiller

```
kubectl config use-context cluster1
helm init
kubectl config use-context cluster2
helm init
```

## Deploy graphite

```
helm --kube-context cluster1 install --name graphite charts/graphite/
```

Get the name of the graphite pod
```
kubectl get pods --namespace default -l "app=graphite,release=graphite"
```

Check the dashboard
```
kubectl port-forward graphite-7665f9cddf-ctxbw 8080

http://127.0.0.1:8080
```
## Deploy Consul

```
helm --kube-context cluster1 install --name consul -f consul_values_cluster1.yaml charts/consul/

helm --kube-context cluster2 install --name consul -f consul_values_cluster2.yaml charts/consul/
```
Check if Consul is working

```
kubectl --context=cluster1 exec consul-0 consul members
kubectl --context=cluster2 exec consul-0 consul members
```

Exchange gossip keys and join WAN gossip pool
```
./setup_consul.sh
```
| see setup_consul.sh for details


