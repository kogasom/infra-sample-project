provider "google" {
  credentials = "${file("account.json")}"
  project     = "${var.gcp_project}"
}

module "cluster1" {
    source = "./k8s-cluster"
    name = "cluster1"
    cluster_name = "cluster1"
    gcp_zone = "europe-west1-b"
}

module "cluster2" {
    source = "./k8s-cluster"
    name = "cluster2"
    cluster_name = "cluster2"
    gcp_zone = "us-west1-a"
}